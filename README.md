# lxf2ldr.html - Introduction

lxf2ldr.html is a standalone HTML/ECMAScript application to convert LEGO
Digital Designer models (.LXF) to LDraw models (.LDR/.MPD).

By “standalone application” we mean that the user just needs to open the file
lxf2ldr.html in their (recent) browser.  No web server needed.  No other
installation than downloading the files.

lxf2ldr.html is a rewriting in JavaScript/ECMAScript of
[lxf2ldr](http://gitlab.com/sylvainld/lxf2ldr/README.md).  It has been written
to circumvent portability issues.

Like its big brother lxf2ldr, lxf2ldr.html is licenced under GPLv3+.


# Requirements

* NONE!

We use [JSZIP](https://stuk.github.io/jszip/) but the necessary file is
included.


# Limitations

Due to local browsing limitations, lxf2ldr.html has less options than
lxf2ldr.  Adding, modifying, or avoiding the conversion of patterns and
flexible parts requires programming skills.
