/*
 *  Copyright 2019  Sylvain Sauvage slswww@free.fr
 *
 *  This program is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU General Public License as
 *  published by the Free Software Foundation; either version 3 of
 *  the License, or (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


// XML parsing
function parse_xml( str ) {
    return (new DOMParser()).parseFromString( str, "text/xml" );
}


// Math stuff
function mat_mul( m, n ) {
    let r =  [ [ 1, 0, 0, 0 ],
               [ 0, 1, 0, 0 ],
               [ 0, 0, 1, 0 ],
               [ 0, 0, 0, 1] ];
    if ( !m ) {
        if ( !n ) {
            return r;
        } else {
            return n;
        }
    } else if ( !n ) {
        return m;
    }
    r[0][0] = m[0][0] * n[0][0] + m[1][0] * n[0][1] + m[2][0] * n[0][2] + m[3][0] * n[0][3];
    r[1][0] = m[0][0] * n[1][0] + m[1][0] * n[1][1] + m[2][0] * n[1][2] + m[3][0] * n[1][3];
    r[2][0] = m[0][0] * n[2][0] + m[1][0] * n[2][1] + m[2][0] * n[2][2] + m[3][0] * n[2][3];
    r[3][0] = m[0][0] * n[3][0] + m[1][0] * n[3][1] + m[2][0] * n[3][2] + m[3][0] * n[3][3];

    r[0][1] = m[0][1] * n[0][0] + m[1][1] * n[0][1] + m[2][1] * n[0][2] + m[3][1] * n[0][3];
    r[1][1] = m[0][1] * n[1][0] + m[1][1] * n[1][1] + m[2][1] * n[1][2] + m[3][1] * n[1][3];
    r[2][1] = m[0][1] * n[2][0] + m[1][1] * n[2][1] + m[2][1] * n[2][2] + m[3][1] * n[2][3];
    r[3][1] = m[0][1] * n[3][0] + m[1][1] * n[3][1] + m[2][1] * n[3][2] + m[3][1] * n[3][3];

    r[0][2] = m[0][2] * n[0][0] + m[1][2] * n[0][1] + m[2][2] * n[0][2] + m[3][2] * n[0][3];
    r[1][2] = m[0][2] * n[1][0] + m[1][2] * n[1][1] + m[2][2] * n[1][2] + m[3][2] * n[1][3];
    r[2][2] = m[0][2] * n[2][0] + m[1][2] * n[2][1] + m[2][2] * n[2][2] + m[3][2] * n[2][3];
    r[3][2] = m[0][2] * n[3][0] + m[1][2] * n[3][1] + m[2][2] * n[3][2] + m[3][2] * n[3][3];

    r[0][3] = m[0][3] * n[0][0] + m[1][3] * n[0][1] + m[2][3] * n[0][2] + m[3][3] * n[0][3];
    r[1][3] = m[0][3] * n[1][0] + m[1][3] * n[1][1] + m[2][3] * n[1][2] + m[3][3] * n[1][3];
    r[2][3] = m[0][3] * n[2][0] + m[1][3] * n[2][1] + m[2][3] * n[2][2] + m[3][3] * n[2][3];
    r[3][3] = m[0][3] * n[3][0] + m[1][3] * n[3][1] + m[2][3] * n[3][2] + m[3][3] * n[3][3];

    return r;
}

function vec_add( u, v ) {
    if ( !u ) {
        if ( !v ) {
            return [ 0, 0, 0 ];
        } else {
            return v;
        }
    } else if ( !v ) {
        return u;
    }
    return [ u[0] + v[0], u[1] + v[1], u[2] + v[2] ];
}

function vec_mul_scal( v, s ) {
    return [ v[0] * s, v[1] * s, v[2] * s ];
}

function mat_mul_vec( m, v ) {
    let x, y, z, w;
    x = v[0] * m[0][0] + v[1] * m[1][0] + v[2] * m[2][0] + m[3][0];
    y = v[0] * m[0][1] + v[1] * m[1][1] + v[2] * m[2][1] + m[3][1];
    z = v[0] * m[0][2] + v[1] * m[1][2] + v[2] * m[2][2] + m[3][2];
    w = v[0] * m[0][3] + v[1] * m[1][3] + v[2] * m[2][3] + m[3][3];
    if ( w == 1.0 ) {
        return [ x, y, z ];
    } else {
        return [ x / w, y / w, z / w ];
    }
}

function rot2mat( x, y, z, angle ) {
    let m = [ [ 1.0, 0.0, 0.0, 0.0 ],
              [ 0.0, 1.0, 0.0, 0.0 ],
              [ 0.0, 0.0, 1.0, 0.0 ],
              [ 0.0, 0.0, 0.0, 1.0 ] ];

    if ( angle == 0.0 ) {
        return m;
    }
    let c, s;
    if ( angle == 90.0 || angle == -270.0 ) {
        s = 1.0;
        c = 0.0;
    } else if ( angle == -90.0 || angle == 270.0 ) {
        s = -1.0;
        c = 0.0;
    } else if ( angle == 180.0 || angle == -180.0 ) {
        s = 0.0;
        c = -1.0;
    } else {
        let a = Math.PI * angle / 180.0;
        c = Math.cos( a );
        s = Math.sin( a );
    }
    if ( x == 0.0 ) {
        if ( y == 0.0 ) {
            if ( z != 0.0 ) {
                // Rotate around the Z axis.
                if ( z < 0 ) {
                    s = -s;
                }
                m[0][0] = c;
                m[1][0] = -s;
                m[0][1] = s;
                m[1][1] = c;

                return m;
            }
        } else if ( z == 0.0 ) {
            // Rotate around the Y axis.
            if ( y < 0 ) {
                s = -s;
            }
            m[2][0] = s;
            m[0][0] = c;
            m[2][2] = c;
            m[0][2] = -s;

            return m;
        }
    } else if ( y == 0.0 && z == 0.0 ) {
        // Rotate around the X axis.
        if ( x < 0 ) {
            s = -s;
        }
        m[1][1] = c;
        m[2][1] = -s;
        m[1][2] = s;
        m[2][2] = c;

        return m;
    }

    let len = x * x + y * y + z * z;
    if ( len != 1.0 && len != 0.0 ) {
        len = Math.sqrt(len);
        x /= len;
        y /= len;
        z /= len;
    }
    let ic = 1.0 - c;
    m[0][0] = x * x * ic + c;
    m[1][0] = x * y * ic - z * s;
    m[2][0] = x * z * ic + y * s;
    m[3][0] = 0.0;
    m[0][1] = y * x * ic + z * s;
    m[1][1] = y * y * ic + c;
    m[2][1] = y * z * ic - x * s;
    m[3][1] = 0.0;
    m[0][2] = x * z * ic - y * s;
    m[1][2] = y * z * ic + x * s;
    m[2][2] = z * z * ic + c;
    m[3][2] = 0.0;
    m[0][3] = 0.0;
    m[1][3] = 0.0;
    m[2][3] = 0.0;
    m[3][3] = 1.0;
    return m;
}

const M4ID = [ [ 1, 0, 0, 0 ],
               [ 0, 1, 0, 0 ],
               [ 0, 0, 1, 0 ],
               [ 0, 0, 0, 1] ];

const LOCAL_ROT = {
    "x":     rot2mat( 1, 0, 0,  90 ),
    "xx":    rot2mat( 1, 0, 0, 180 ),
    "xxx":   rot2mat( 1, 0, 0, -90 ),
    "y":     rot2mat( 0, 1, 0,  90 ),
    "yy":    rot2mat( 0, 1, 0, 180 ),
    "yyy":   rot2mat( 0, 1, 0, -90 ),
    "y7p/6": rot2mat( 0, 1, 0, 210 ),
    "yspec": rot2mat( 0, 1, 0, 230 ),
    "z":     rot2mat( 0, 0, 1,  90 ),
    "zz":    rot2mat( 0, 0, 1, 180 ),
    "zzz":   rot2mat( 0, 0, 1, -90 )
};


// Conversion tables
// reading
function load_ldraw_xml() {
    let ldrawmapping = parse_xml( ldraw_xml_p() );
    if ( ldrawmapping.documentElement.nodeName != "LDrawMapping" ) {
        throw new Error( "Expecting element LDrawMapping, got " + ldrawmapping.documentElement.nodeName );
    }
    let materials = [];
    let bricks = [];
    let transformations = [];
    let elts = ldrawmapping.getElementsByTagName( "Material" );
    for ( let i = 0; i < elts.length; i++ ) {
        let elt = elts[i];
        materials[ elt.getAttribute( "lego" ) ] = elt.getAttribute( "ldraw" );
    }
    elts = ldrawmapping.getElementsByTagName( "Brick" );
    for ( let i = 0; i < elts.length; i++ ) {
        let elt = elts[i];
        bricks[ elt.getAttribute( "lego" ) ] = elt.getAttribute( "ldraw" );
    }
    elts = ldrawmapping.getElementsByTagName( "Transformation" );
    for ( let i = 0; i < elts.length; i++ ) {
        let elt = elts[i];
        transformations[ elt.getAttribute( "ldraw" ) ] = {
            translation: [ -+elt.getAttribute( "tx" ),
                           -+elt.getAttribute( "ty" ),
                           -+elt.getAttribute( "tz" ) ],
            rotation: rot2mat( +elt.getAttribute( "ax" ),
                               +elt.getAttribute( "ay" ),
                               +elt.getAttribute( "az" ),
                               -180.0 * +elt.getAttribute( "angle" ) / Math.PI )
        };
    }
    return {
        materials: materials,
        bricks: bricks,
        transformations: transformations
    };
}

function make_transformation( txt ) {
    if ( !txt ) {
        return null;
    }
    let values = txt.split( "," );
    return {
        translation: [ +values[0], +values[1], +values[2] ],
        rotation: rot2mat( +values[3], +values[4], +values[5], +values[6] )
    };
}

function make_sub( txt ) {
    if ( !txt ) {
        return null;
    }
    let sp = txt.indexOf( " " );
    return {
        datfile: txt.substring( 0, sp ).toLowerCase(),
        transformation: make_transformation( txt.substring( sp+1 ) )
    };
}

function make_counted( txt ) {
    if ( !txt ) {
        return null;
    }
    let sp = txt.indexOf( " " );
    return {
        count: +txt.substring( 0, sp ),
        transformation: make_transformation( txt.substring( sp+1 ) )
    };
}

function make_bicounted( txt ) {
    if ( !txt ) {
        return null;
    }
    let sp = txt.indexOf( " " );
    let counts = txt.substring( 0, sp ).split( "," );
    return {
        before: +counts[0],
        after: +counts[counts.length-1],
        transformation: make_transformation( txt.substring( sp+1 ) )
    };
}

function make_flex( hash ) {
    let result = {};
    Object.keys( hash ).forEach( function ( id ) {
        let flex = hash[id];
        result[id] = {
            type: flex["type"],
            body: make_bicounted( flex["body"] ),
            head: make_counted( flex["head"] ),
            tail: make_counted( flex["tail"] ),
            head_plus: make_sub( flex["head+"] ),
            tail_plus: make_sub( flex["tail+"] )
        };
    });
    return result;
}

function make_decors( hash ) {
    let result = {};
    Object.keys( hash ).forEach( function ( id ) {
        let decs = hash[id];
        if ( !decs["colors"] ) {
            decs["colors"] = [];
        }
        decs["colors"].forEach( function( place ) {
            if ( place ) {
                Object.keys( place ).forEach( function ( col ) {
                    let vals = place[col].split( " " );
                    place[col] = {
                        datfile: vals[0].toLowerCase(),
                        overwrite: ( vals[1] == "OW" )
                    };
                } );
            }
        } );
        if ( !decs["decorations"] ) {
            decs["decorations"] = {};
        }
        let combis = decs["decorations"];
        Object.keys( combis ).forEach( function( combi ) {
            let vals = combis[combi].split( " " );
            if ( vals.length > 1 ) {
                combis[combi] = {
                    datfile: vals[0].toLowerCase(),
                    transformation: {
                        translation: [ 0, 0, 0 ],
                        rotation: LOCAL_ROT[ vals[1] ]
                    }
                };
            } else {
                combis[combi] = {
                    datfile: vals[0].toLowerCase(),
                    transformation: null
                };
            }
        } );
        if ( !decs["usecolor"] ) {
            decs["usecolor"] = 0;
        }
        result[id] = {
            usecolor: decs["usecolor"],
            colors: decs["colors"],
            decorations: combis
        };
    } );
    return result;
}

// Conversion tables
// using
const DECORS = make_decors( decors_lxf2ldr_jsonp() );

const FLEX = make_flex( flex_lxf2ldr_jsonp() );

const LDRAW_XML = load_ldraw_xml();


function ldraw_xml_x2l_id( lddid ) {
    return LDRAW_XML.bricks[ lddid ] || ( lddid + ".dat" );
}

function ldraw_xml_x2l_tr( lid ) {
    return LDRAW_XML.transformations[ lid ];
}

function ldraw_xml_x2l_color( c ) {
    return LDRAW_XML.materials[c] || c;
}

function decors_x2l_decor( lego, decorations, colors ) {
    let decor = DECORS[lego];
    if ( !decor ) {
        return [ 0, { datfile: null, transformation: null } ];
    }

    let sub = {
        datfile: null,
        transformation: null
    };
    let maxcol = Math.min( colors.length, decor.colors.length );
    for ( let i = 0; i < maxcol; i++ ) {
        let curcol = colors[i] == 0 ? colors[0] : colors[i];
        if ( decor.colors[i] ) {
            let ssub = decor.colors[i][curcol];
            if ( ssub && ( !sub.datfile || ssub.overwrite ) ) {
                sub = {
                    datfile: ssub.datfile,
                    transformation: null
                };
            }
        }
    }
    if ( decor.decorations[ decorations ] ) {
        sub = decor.decorations[ decorations ];
    }
    return [ decor.usecolor, sub ];
}

function flex_is_flexible( lddid ) {
    return FLEX[lddid];
}

function flex_x2l_flex( lddid ) {
    return FLEX[lddid];
}


// LXF reading
function recenter_part( part, center ) {
    part.positions = part.positions.map ( function( p ) {
        return [ p[0] - center[0], p[1] - center[1], p[2] - center[2] ];
    } );
}

function recenter_group( group, center ) {
    // make everyone relative to me
    group.parts.forEach( function( p ) {
        recenter_part( p, group.translation );
    } );
    group.groups.forEach( function( g ) {
        recenter_group( g, group.translation );
    } );
    // make me relative to daddy
    group.translation = [ group.translation[0] - center[0],
                          group.translation[1] - center[1],
                          group.translation[2] - center[2] ];
}

function read_group( group, bname, num, siblings, m_refs, m_all_groups ) {
    let name = bname + "_" + num;
    let parts = [];
    let sons = [];
    let flex = 0;
    let srefs = group.getAttribute( "partRefs" );
    if ( srefs ) {
        let refs = srefs.split( "," );
        refs.forEach( function( ref ) {
            let p = m_refs[ref];
            m_refs[ref] = null;
            if ( p.positions.length == 1 ) {
                parts.push( p );
            } else { // flexible part, putting it in its own group
                let g = {
                    name: name + "_flexible_" + flex++,
                    parts: [ p ],
                    groups: [],
                    translation: p.positions[0]
                };
                m_all_groups.push( g );
                sons.push( g );
            }
        } );
    }
    // inner groups
    let elts = group.children;
    for ( let i = 0, elt; elt = elts[i]; i++ ) {
        if ( elt.nodeName == "Group" ) {
            read_group( elt, name, i, sons, m_refs, m_all_groups );
        }
    }
    let position = [ 0, 0, 0 ];
    if ( parts.length == 0 ) {
        if ( sons.length == 0 ) {
            // empty group
        } else {
            position = sons[0].translation;
        }
    } else {
        position = parts[0].positions[0];
    }
    let g = {
        name: name,
        parts: parts,
        groups: sons,
        translation: position
    };
    m_all_groups.push( g );
    siblings.push( g );
}

function manage_assemblies( group, m_map_assemblies ) {
    let to_remove = [];
    let to_keep = [];
    let assemblies = [];

    group.parts.forEach( function( p ) {
        let assembly = m_map_assemblies[ p.ref ];
        if ( typeof assembly !== 'undefined' ) {
            to_remove.push( p );
            if ( assembly != null ) {
                assemblies.push( assembly );
                // we won’t add this assembly anymore but parts are still
                // to be removed, so refs aren’t removed, they are nulled
                assembly.parts.forEach( function( q ) {
                    m_map_assemblies[ q.ref ] = null;
                } );
            }
        } else {
            to_keep.push( p );
        }
    } );

    if ( to_remove.length ) {
        group.parts = to_keep;
        group.groups = group.groups.concat( assemblies );
    }
}

// Conversion

function ldr_pos2str( pos ) {
    return pos.join( " " );
}

function ldr_mat2str( mat ) {
    return mat[0][0] + " " + mat[1][0] + " " + mat[2][0] + " " + mat[0][1] + " " + mat[1][1] + " " + mat[2][1] + " " + mat[0][2] + " " + mat[1][2] + " " + mat[2][2];
}

function ldr_use( color, position, rotation, dat ) {
    return "1 " + color + " " + ldr_pos2str( position ) + " " + ldr_mat2str( rotation ) + " " + dat;
}

function ldr_com( position, rotation ) {
    return "0 LXF2LDR FLEXNODE " + ldr_pos2str( position ) + " " + ldr_mat2str( rotation );
}


const AXES = rot2mat( 1, 0, 0, 180 );
const ZERO = [ 0, 0, 0 ];

function ldr( color, lid, ldd_pos, ldd_rot, x2l_tr, local_tr, commented = false ) {
    let rot = x2l_tr ? ( mat_mul( ldd_rot, x2l_tr.rotation ) ) : ldd_rot;
    let ldr_rot = mat_mul( mat_mul( AXES,
                                    local_tr ? mat_mul( rot, local_tr.rotation ) : rot ),
                           AXES );
    let move = vec_add( local_tr ? local_tr.translation : ZERO,
                        x2l_tr ? x2l_tr.translation : ZERO );
    let ldr_pos = vec_mul_scal( mat_mul_vec( AXES,
                                             vec_add( ldd_pos,
                                                      mat_mul_vec( rot, move ) ) ),
                                25 );

    if ( commented ) {
        return ldr_com( ldr_pos, ldr_rot );
    } else {
        return ldr_use( color, ldr_pos, ldr_rot, lid );
    }
}

const EMPTY_DECOR = /\A0(,0)*\Z/;

function part2ldr( ldd, output, mark_color ) {
    // translate id
    let lid = ldraw_xml_x2l_id( ldd.id );

    // transformation for that part
    let x2l_tr = ldraw_xml_x2l_tr( lid );

    // substitute part and color for decorations
    let us = decors_x2l_decor( ldd.id, ldd.decors, ldd.colors );
    let usecolor = us[0];
    let sub = us[1];
    if ( sub.datfile ) {
        lid = sub.datfile;
    }
    let main_color = ldraw_xml_x2l_color( ldd.colors[0] );
    let colors = ldd.colors.map( function( c ) {
        return c ? ldraw_xml_x2l_color( c ) : main_color; // 0 means default
    } );
    main_color = colors[usecolor]; // ldr default isn’t first

    // marks unmatched decorated parts
    if ( mark_color && ldd.decors && !sub.datfile ) {
        if ( !EMPTY_DECOR.test( ldd.decors ) ) {
            main_color = mark_color;
        }
    }

    if ( ldd.rotations.length == 1 ) {        // unflexible part
        output.push( ldr( main_color, lid, ldd.positions[0], ldd.rotations[0], x2l_tr, sub.transformation ) );
        output.push( "\r\n" );
    } else {                                  // flexible part, not flexed
        output.push( "0 LXF2LDR BEGIN FLEXIBLE PART\r\n" );

        if ( !flex_is_flexible( ldd.id ) ) { // not known
            // first bone is the part
            output.push( ldr( main_color, lid, ldd.positions[0], ldd.rotations[0], x2l_tr, sub.transformation ) );
            output.push( "\r\n" );
            // remaining bones are commented
            for ( let i = 1; i < ldd.positions.length; i++ ) {
                output.push( ldr( main_color, "", ldd.positions[i], ldd.rotations[i], x2l_tr, sub.transformation, true ) );
                output.push( "\r\n" );
            }
        } else {                            // known flexible part
            let flex = flex_x2l_flex( ldd.id );

            // if there are two colors, use the second one for the ends
            // (only one flexible part does that)
            let color_ends = main_color;
            if ( colors.length > 1 ) {
                color_ends = colors[1];
            }

            // supplemental head
            if ( flex.head_plus ) {
                output.push( ldr( color_ends, flex.head_plus.datfile, ldd.positions[0], ldd.rotations[0], null, flex.head_plus.transformation ) );
                output.push( "\r\n" );
            }

            // LSynth commands
            output.push( "0 SYNTH BEGIN " + flex.type + " " + main_color + "\r\n" );
            let max_count = ldd.positions.length;
            // replacing head bones with (green) constraints
            if ( flex.head ) {
                for ( let i = 0; i < flex.head.count; i++ ) {
                    output.push( ldr( 2, "ls01.dat", ldd.positions[i], ldd.rotations[i], null, flex.head.transformation ) );
                    output.push( "\r\n" );
                }
            }
            // replacing body bones with constraints
            if ( flex.body ) {
                for ( let i = flex.body.before; i < max_count - flex.body.after; i++ ) {
                    output.push( ldr( main_color, "ls01.dat", ldd.positions[i], ldd.rotations[i], null, flex.body.transformation ) );
                    output.push( "\r\n" );
                }
            }
            // replacing tail bones with (red) constraints
            if ( flex.tail ) {
                for ( let i = max_count - flex.tail.count; i < max_count; i++ ) {
                    output.push( ldr( 4, "ls01.dat", ldd.positions[i], ldd.rotations[i], null, flex.tail.transformation ) );
                    output.push( "\r\n" );
                }
            }
            output.push( "0 SYNTH END\r\n" );

            // supplemental tail
            if ( flex.tail_plus ) {
                output.push( ldr( color_ends, flex.tail_plus.datfile, ldd.positions[ldd.positions.length - 1], ldd.rotations[ldd.rotations.length - 1], null, flex.tail_plus.transformation ) );
                output.push( "\r\n" );
            }
        }
        output.push( "0 LXF2LDR END FLEXIBLE PART\r\n" );
    }
}

function group2ldr( group, output, mark_color ) {
    output.push( "0 FILE " + group.name + "\r\n" );
    group.parts.forEach( function( p ) {
        part2ldr( p, output, mark_color );
    } );
    group.groups.forEach( function( g ) {
        output.push( ldr( 16, g.name, g.translation, M4ID, 0, 0 ) );
        output.push( "\r\n" );
    } );
    output.push( "\r\n" );
}

function lxf2ldr( m_name, lxfml, mark_color, finished_cb ) {
    let lxf = parse_xml( lxfml ).documentElement;

    // read bricks
    let m_refs = {};
    let m_map_assemblies = {};
    let m_all_assemblies = [];
    let m_all_parts = [];
    let m_all_groups = [];

    let elts = lxf.querySelectorAll( "Bricks > Brick" );
    for ( let i = 0, elt; elt = elts[i]; i++ ) { // a brick is composed of parts
        let pts = elt.getElementsByTagName( "Part" );
        let read_parts = [];
        for ( let j = 0, pt; pt = pts[j]; j++ ) {
            let id = pt.getAttribute( "designID" );
            let decs = pt.getAttribute( "decoration" );
            let cols = pt.getAttribute( "materials" ).split( "," );
            let rid = pt.getAttribute( "refID" );
            let bones = pt.getElementsByTagName( "Bone" );
            let poss = [];
            let rots = [];
            for ( let k = 0, b; b = bones[k]; k++ ) {
                let trans = b.getAttribute( "transformation" ).split( "," );
                poss.push( [ +trans[9], +trans[10], +trans[11] ] );
                rots.push( [ [ +trans[0], +trans[1], +trans[2], 0 ],
                             [ +trans[3], +trans[4], +trans[5], 0 ],
                             [ +trans[6], +trans[7], +trans[8], 0 ],
                             [         0,         0,         0, 1 ] ] );
            }
            let part = {
                ref: rid,
                id: id,
                colors: cols,
                decors: decs,
                positions: poss,
                rotations: rots
            };
            m_all_parts.push( part );
            m_refs[ rid ] = part;
            read_parts.push( part );
        }
        if ( read_parts.length > 1 ) { // more than one part read => assembly
            // create a group/assembly
            let assembly = {
                name: ( m_name + "_assembly_" + m_all_assemblies.length ),
                parts: read_parts,
                groups: [],
                translation: read_parts[0].positions[0]
            };
            // reference the assembly
            m_all_assemblies.push( assembly );
            read_parts.forEach( function( p ) {
                m_map_assemblies[ p.ref ] = assembly;
            } );
        }
    }

    // read groups
    let groups0 = [];
    elts = lxf.querySelectorAll( "GroupSystems > GroupSystem > Group" ); // first level groups
    for ( let i = 0, elt; elt = elts[i]; i++ ) {
        read_group( elt, m_name, i, groups0, m_refs, m_all_groups );
    }

    // put all parts not already in a group in the main group
    let parts = [];
    let flex = 0;
    Object.keys( m_refs ).forEach( function ( rid ) {
        let p = m_refs[rid];
        if ( p ) {
            if ( p.positions.length == 1 ) {
                parts.push( p );
            } else { // flexible part, putting it in its own group
                let g = {
                    name: m_name + "_flexible_" + flex++,
                    parts: [ p ],
                    groups: [],
                    translation: p.positions[0]
                };
                m_all_groups.push( g );
                groups0.push( g );
            }
        }
    });

    // remove assembly-subparts from their groups and put the assembly-group
    // instead
    m_all_groups.forEach( function( g ) {
        manage_assemblies( g, m_map_assemblies );
    } );

    // create main group
    let m_group = {
        name: m_name,
        parts: parts,
        groups: groups0,
        translation: [ 0, 0, 0 ]
    };
    manage_assemblies( m_group, m_map_assemblies );

    // recenter all groups on their position
    let position = [ 0, 0, 0 ];
    if ( !parts.length ) {
        if ( !groups0.length ) {
            // no groups, no parts
        } else {
            position = groups0[0].translation;
        }
    } else {
        position = parts[0].positions[0];
    }
    recenter_group( m_group, position );

    // do convert
    let output = [];

    // output main group
    group2ldr( m_group, output, mark_color );

    // output groups contents
    m_all_groups.forEach( function( g ) {
        group2ldr( g, output, mark_color );
    } );
    // output assemblies contents
    m_all_assemblies.forEach( function( a ) {
        group2ldr( a, output, mark_color );
    } );
    output.push( "\r\n" );

    // all done
    finished_cb( output.join( "" ) );
}

// main conversion function
function lxf2ldr_convert( file, mark_color, finished_cb ) {

    // read the file
    let s = file.name.lastIndexOf( "/" );
    let m_name = file.name.substring( s+1 ).replace( " ", "_" );
    
    JSZip.loadAsync( file ).then( function( zip ) {
        zip.file( "IMAGE100.LXFML" ).async( "string" ).then( function( lxfml ) {
            lxf2ldr( m_name, lxfml, mark_color, finished_cb );
        } );
    }, function( error ) {
        throw new Error( error );
    } );
}
